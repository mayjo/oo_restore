#!/usr/bin/python
# -*- coding: utf-8 -*-

import subprocess, os, re, shutil

def setup_deps_debian(git_dir):
    debian_control_path = os.path.join(git_dir, 'debian/control')
    debian_control = open(debian_control_path).read()
    debs = re.findall('python-[0-9a-z]+',debian_control)
    debs += ["postgresql"]
    proc = subprocess.call(['sudo','apt-get','-y','install'] + debs)

'''
 *** si pb locales, il faudra ajouter commande "dpkg-reconfigure locales" pour configurer fr-FR.UTF-8
dpkg-reconfigure locales

 *** + config date :
mv /etc/localtime /etc/localtime.old
ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime


 *** si Apache installé, il faut executer les commandes :
service apache2 stop
apt-get -y remove apache2*
apt-get autoremove
'''

print "*** apt-get update"
cmd = ['apt-get','update']
subprocess.call(cmd)

print "*** apt-get -y upgrade"
cmd = ['apt-get','-y','upgrade']
subprocess.call(cmd)

print "*** apt-get -y install python-pip at sudo zip node-less xfonts-75dpi xfonts-base"
cmd = ['apt-get','-y','install', 'python-pip','at', 'sudo', 'zip', 'node-less', 'xfonts-75dpi', 'xfonts-base']
subprocess.call(cmd)

print "*** pip install phonenumbers psycogreen"
cmd = ['pip', 'install', 'phonenumbers', 'psycogreen']
subprocess.call(cmd)

print "*** useradd -m -g sudo -s /bin/bash odoo"
cmd = ['useradd', '-m', '-g', 'sudo', '-s', '/bin/bash', 'odoo']
subprocess.call(cmd)

#print "*** passwd odoo - indiquer le même mot de passe que root"
#cmd = ['passwd', 'odoo']
#subprocess.call(cmd)

print "*** su - odoo 'git clone https://github.com/odoo/odoo.git -b 9.0'"
cmd=['su','-', 'odoo','-c','git clone https://github.com/odoo/odoo.git -b 9.0']
subprocess.call(cmd)

print "*** dependance python"
#cmd = ['su','-','odoo','./odoo/odoo.py setup_deps']
#subprocess.call(cmd)
setup_deps_debian('/home/odoo/odoo')

print "*** creation user odoo dans postgresql"
#cmd = ['su','-','odoo','-c','./odoo/odoo.py setup_pg']
cmd = ['sudo','su','-','postgres','-c','createuser -s odoo']
subprocess.call(cmd)

os.chdir('/tmp')
print "*** installation wkhtmltopdf"
cmd = ['wget','http://download.gna.org/wkhtmltopdf/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-jessie-amd64.deb']
subprocess.call(cmd)
cmd = ['dpkg', '-i', 'wkhtmltox-0.12.2.1_linux-jessie-amd64.deb']
subprocess.call(cmd)

shutil.copyfile('/usr/local/bin/wkhtmltopdf','/usr/bin/wkhtmltopdf')
shutil.copyfile('/usr/local/bin/wkhtmltoimage','/usr/bin/wkhtmltoimage')

print "*** config lancement Odoo au reboot"
os.remove('/etc/rc.local')
f = open('/etc/rc.local','w')
f.write("#!/bin/sh -e\n")
#f.write('su - odoo -c "/home/odoo/odoo/odoo.py --logfile=/home/odoo/odoo.log  > /dev/null 2>&1 &"\n')
f.write('su - odoo -c "/home/odoo/lance_odoo.sh"\n')

f.write("exit 0")
f.close()

cmd = ['chmod','+x','/etc/rc.local']
subprocess.call(cmd)

print "*** Odoo sera lancé à chaque reboot du serveur"
shutil.copy('/tmp/oo_restore/lance_odoo.sh','/home/odoo/.')
cmd = ['chown','odoo:sudo','/home/odoo/lance_odoo.sh']
subprocess.call(cmd)

# pas testé
print "*** modification crontab de odoo - purge log"
cmd=['su','-', 'odoo','-c','(crontab -l; echo "0 23 * * * find /home/odoo/log -name odoo.log.* -mtime +7 -exec rm {} \;")|crontab -']
subprocess.call(cmd)

print "*** modification crontab de odoo - relance Odoo"
cmd=['su','-', 'odoo','-c','(crontab -l; echo "0 5 * * 7 /home/odoo/lance_odoo.sh")|crontab -']
subprocess.call(cmd)







