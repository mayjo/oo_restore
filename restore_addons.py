#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from ftplib import FTP
import zipfile
import subprocess

def rech_addons(arg):
    ftp = FTP(site)
    ftp.login(utilisateur, motdepasse)
    data = []
    cr = ftp.dir('-t',data.append)
    for line in data:
        l = line.split(" ")
        if arg == 1 and l[-1].startswith('_home_odoo_.local_share_Odoo_addons') :
            ftp.quit()
            return l[-1]
        if arg == 2 and l[-1].startswith('_home_odoo_extra-addons') :
            ftp.quit()
            return l[-1]
    ftp.quit()
    return False

def get_zip(arg,f_name):
    os.chdir(f_dir)
    ftp = FTP(site)
    ftp.login(utilisateur, motdepasse)
    with open(f_name, 'wb') as f:
        ftp.retrbinary('RETR ' + f_name, f.write)
    ftp.quit()

def unzip_f(f_name):
    os.chdir(f_dir)
    with zipfile.ZipFile(f_name, "r") as z:
        z.extractall(f_dir_unzip)

f_dir='/tmp'
f_dir_unzip = "/"

# recuperer les infos utilisateur et motdepasse
utilisateur = raw_input('Entrez votre_user :')
motdepasse = raw_input("Entrez votre mot de passe :")
site = "ftp.agipme.fr"

# recuperer les 2 fichiers zip dans /tmp
get_zip(1,rech_addons(1))
get_zip(2,rech_addons(2))

# unzip des fichiers
unzip_f(rech_addons(1))
unzip_f(rech_addons(2))

# mettre droits à odoo:odoo
cmd = ['chown','-R','odoo:odoo','/home/odoo/extra-addons']
subprocess.call(cmd)
cmd = ['chown','-R','odoo:odoo','/home/odoo/.local']
subprocess.call(cmd)




