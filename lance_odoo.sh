#!/bin/bash

F_PID="/home/odoo/odoo-server.pid"
if [ -f $F_PID ]
then
    PID=`cat $F_PID`
    if [ -e /proc/$PID ]
    then
        kill $PID 
    fi
fi

sleep 2

F_ODOO="/home/odoo/openerp-server.conf"
if [ -f $F_ODOO ]
then
    /home/odoo/odoo/odoo.py -c $F_ODOO  >/dev/null 2>&1 &
else
    /home/odoo/odoo/odoo.py >/dev/null 2>&1 &
fi
echo $! > /home/odoo/odoo-server.pid
